//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Equipos
var EquiposSchema = new Schema({
Placa_Inventario: {type:String, required:true}, 
Nombre: {type:String, required:true}, 
Descripcion: {type:String, required:true}, 
Codigo_QR: {type:String, required:true}, 
Ubicacion: {type:String, required:true}, 

});

EquiposSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Equipos', EquiposSchema);