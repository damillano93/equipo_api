//importar modelo
var Equipos = require('../models/equipos');

//funcion create
exports.equipos_create = function (req, res) {
    var equipos = new Equipos(
        req.body
    );

    equipos.save(function (err, equipos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: equipos._id})
    })
};
//funcion read by id
exports.equipos_details = function (req, res) {
    Equipos.findById(req.params.id, function (err, equipos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(equipos);
    })
};
//funcion read all
exports.equipos_all = function (req, res) {
    Equipos.find(req.params.id, function (err, equipos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(equipos);
    })
};
//funcion update
exports.equipos_update = function (req, res) {
    Equipos.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, equipos) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", equipos: equipos });
    });
};

//funcion delete
exports.equipos_delete = function (req, res) {
    Equipos.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};